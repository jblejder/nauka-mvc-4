﻿using System;

namespace NaukaMVC4.Models
{
    public class Bids
    {
        public DateTime DatePlaced { get; set; }
        public Member Member { get; set; }
        public decimal BidAmount { get; set; }
    }
}