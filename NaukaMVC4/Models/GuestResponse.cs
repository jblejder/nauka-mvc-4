﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace NaukaMVC4.Models
{
    public class GuestResponse
    {
        [Required(ErrorMessage ="Pleas enter yout name")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Pleas enter your email address")]
        [RegularExpression(".+\\@.\\..+")]
        public string Email { get; set; }
        [Required(ErrorMessage ="Please enter yout phone number")]
        public string Phone { get; set; }
        [Required(ErrorMessage ="Pleas specify whether you'll attend")]
        public bool? WillAtend { get; set; }
    }
}