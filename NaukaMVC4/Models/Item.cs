﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaukaMVC4.Models
{
    public class Item
    {
        public int ItemID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime AuctionEndDate { get; set; }
        public IList<Bids> Bids { get; set; }

        public Item()
        {
            Bids = new List<Bids>();
        }

        public void AddBid(Member memberParam, decimal amountParam)
        {
            if (Bids.Count == 0 || amountParam > Bids.Max(e => e.BidAmount))
                Bids.Add(new Bids()
                {
                    BidAmount = amountParam,
                    DatePlaced = DateTime.Now,
                    Member = memberParam
                });
            else
                throw new InvalidOperationException("Bid amount too low");
        }
    }
}