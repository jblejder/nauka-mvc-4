﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssentialToolsForMvc.Models
{
    public class LinqValueCalculator : IValueCalculator
    {
        IDiscountHelper discounter;

        public LinqValueCalculator(IDiscountHelper discountParam)
        {
            discounter = discountParam;
        }

        public decimal ValueProducts(IEnumerable<Product> Products)
        {
            return discounter.ApplyDiscount(Products.Sum(p => p.Price));
        }
    }
}