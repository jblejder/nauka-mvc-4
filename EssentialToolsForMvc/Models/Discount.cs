﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssentialToolsForMvc.Models
{
    public interface IDiscountHelper
    {
        decimal ApplyDiscount(decimal totalParam);
    }

    public class DefaultDiscounter : IDiscountHelper
    {
        public decimal DiscountSize { get; set; }

        public DefaultDiscounter(decimal discountParam)
        {
            DiscountSize = discountParam;
        }

        public decimal ApplyDiscount(decimal totalParam)
        {
            return (totalParam - (DiscountSize / 100m) * totalParam);
        }
    }
}