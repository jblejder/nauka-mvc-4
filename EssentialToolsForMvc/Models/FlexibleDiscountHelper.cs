﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssentialToolsForMvc.Models
{
    public class FlexibleDiscountHelper : IDiscountHelper
    {
        public decimal ApplyDiscount(decimal totalPatam)
        {
            decimal discount = totalPatam > 100 ? 70 : 25;
            return (totalPatam - (discount / 100m * totalPatam));
        }
    }
}