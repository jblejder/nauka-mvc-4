﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NaukaMVC4.Models;

namespace NaukaMVC4.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CanAddBid()
        {
            // Arrange
            Item target = new Item();
            Member memberParam = new Member();
            decimal amountPrice = 150M;

            // Act
            target.AddBid(memberParam, amountPrice);

            // Assert
            Assert.AreEqual(1, target.Bids.Count);
            Assert.AreEqual(amountPrice, target.Bids[0].BidAmount);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CannotAddLoverBid()
        {
            // Arrange
            Item target = new Item();
            Member memberParam = new Member();
            decimal amountParam = 150M;

            // Act
            target.AddBid(memberParam, amountParam);
            target.AddBid(memberParam, amountParam - 10);

        }

        [TestMethod]
        public void CanAddHigherBid()
        {
            // Arrange
            Item target = new Item();
            Member memberParam = new Member();
            decimal amountParam = 150M;

            // Act
            target.AddBid(memberParam, amountParam);
            target.AddBid(memberParam, amountParam + 10);

            // Assert
            Assert.AreEqual(2, target.Bids.Count);
            Assert.AreEqual(amountParam+10, target.Bids[1].BidAmount);
        }
    }
}
